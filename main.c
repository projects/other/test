#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/syscall.h>

int main (int argc, char *argv[])
{
    pid_t cpid = getpid(); 
    pid_t pid = fork();
    cpid = getpid(); 

    if (pid < 0)
    {
        fprintf(stderr, "fork: %s", strerror(errno));
        return 1;
    }

    if (pid == 0)
    {
        //cpid = getpid();//  = syscall(__NR_getpid);
        printf("Child process with pid=%d\n", cpid);
        sleep(10);
        return 0;
    }

    printf("Main process with pid=%d\n", cpid);
    sleep(10);

    return 0;
}

